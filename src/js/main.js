import styles from './../css/main.css';
import 'chartjs-plugin-colorschemes';

// NOTE: TO use Jquery, just call the modules you want
// var $ = require('jquery/src/core');
// require('jquery/src/core/init');
// require('jquery/src/manipulation');

// OR, use all of them
var $ = require('jquery/src/jquery');
var Chart = require('chart.js/dist/Chart.bundle');
// And write your code
// $('body').append('<p>Jquery is working</p>');
console.log("ready!");
